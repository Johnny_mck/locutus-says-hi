""" RUN.py
runfile for robot game project, ludum dare #27

TODO:
    * stuff
"""

import pygame
import sprites
import sys

from pygame.locals import *
from pygame.key import *
from maps import mapx, x, y

# CONSTANTS
# dimentions
WIDTH = 640
HEIGHT = 480

# colours
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

pygame.init()

display = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Platformer Engine - Test")

# entities
background = pygame.Surface(display.get_size())
character = sprites.BaseSprite()
platforms = []
entities = pygame.sprite.Group()
for row in mapx:
    for col in row:
        if col == "#":
            t = sprites.BaseTile(x, y)
            platforms.append(t)
            entities.add(t)
        x += 50 
    y += 50
    x = 0
entities.add(character)    
clock = pygame.time.Clock()

def main():
    up = down = left = right = False
    while True:
        clock.tick(30)
        for e in pygame.event.get():
            if e.type == QUIT:
                sys.exit(0)
            if e.type == KEYDOWN and e.key == K_w:
                up = True
            if e.type == KEYDOWN and e.key == K_s:
                down = True
            if e.type == KEYDOWN and e.key == K_d:
                right = True
            if e.type == KEYDOWN and e.key == K_a:
                left = True
            if e.type == KEYUP and e.key == K_w:
                up = False 
            if e.type == KEYUP and e.key == K_s:
                down = False 
            if e.type == KEYUP and e.key == K_d:
                right = False 
            if e.type == KEYUP and e.key == K_a:
                left = False 
            if e.type == KEYUP and e.key == K_SPACE:
                character.rect.centerx = 300
                character.rect.centery = 10
            
        background.fill(WHITE)
        display.blit(background, (0, 0))

        entities.clear(display, background)
        entities.draw(display)
        character.update(left, right, up, down, platforms)

        pygame.display.flip()

if __name__ == "__main__":
    main()
