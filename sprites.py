import pygame

class BaseTile(pygame.sprite.Sprite):
    def __init__(self, x=300, y=300):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 50))
        self.image.fill((0, 255, 0))
        self.rect = self.image.get_rect()
        self.rect.x = x 
        self.rect.y = y

class BaseSprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((50, 50))
        self.image.fill((255, 0, 0))
        self.rect = self.image.get_rect()
        self.on_ground = False
        self.rect.centerx = 50
        self.rect.centery = 50
        self.dx = 5
        self.dy = 5

    def collide(self, dx, dy, platforms):
        for i in platforms:
            if pygame.sprite.collide_rect(self, i):
                if dx > 0:
                    self.rect.right = i.rect.left
                if dx < 0:
                    self.rect.left = i.rect.right
                if dy > 0:
                    self.rect.bottom = i.rect.top
                    self.on_ground = True
                    self.dy = 0
                if dy < 0:
                    self.rect.top = i.rect.bottom
                    self.dy = 0

    def update(self, left, right, up, down, platforms):
        if left:
            self.dx = -5
        if right:
            self.dx = 5
        if up:
            if self.on_ground:
                self.dy -= 8
        if down:
            pass
        if not self.on_ground:
            self.dy += 0.5
            if self.dy > 100: self.dy = 100
        if not(left or right):
            self.dx = 0
        self.rect.left += self.dx
        self.collide(self.dx, 0, platforms)
        self.rect.top += self.dy
        self.on_ground = False
        self.collide(0, self.dy, platforms)
